import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'tdm-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.scss']
})
export class InputsComponent {
  public form: FormGroup;
  constructor(private fb: FormBuilder) {
    this.form = fb.group({
      testOne: null
    });
  }
}
