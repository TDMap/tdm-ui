import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from 'src/lib';
import { DeviderModule } from 'src/lib';
import { InputModule } from 'src/lib';
import { TextAreaModule } from 'src/lib';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonsComponent } from './buttons/buttons.component';
import { InputsComponent } from './inputs/inputs.component';

const TDM_MODULES = [ButtonModule, DeviderModule, InputModule, TextAreaModule];

@NgModule({
  declarations: [AppComponent, ButtonsComponent, InputsComponent],
  imports: [CommonModule, BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule, ...TDM_MODULES],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
