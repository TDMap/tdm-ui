import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, Input, Renderer2, ViewEncapsulation } from '@angular/core';

export type ButtonSize = 'default' | 'large';
export type ButtonType = 'default' | 'light' | 'dark' | 'warn' | 'success';

@Component({
  selector: 'button[tdm-button], a[tdm-button]',
  exportAs: 'tdmButton',
  preserveWhitespaces: false,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  template: `
    <ng-content></ng-content>
  `,
  host: {
    '[class.button-default]': `type === 'default'`,
    '[class.button-warn]': `type === 'warn'`,
    '[class.button-success]': `type === 'success'`,

    '[class.button-default-size]': `size === 'default'`,
    '[class.button-large-size]': `size === 'large'`,

    '[attr.disabled]': 'disabled || null',
    '[class.disabled]': `disabled || null`,

    class: 'tdm-button'
  }
})
export class ButtonComponent implements AfterViewInit {
  @Input() size: ButtonSize = 'default';
  @Input() type: ButtonType = 'default';
  @Input() disabled: boolean = false;

  constructor(private elementRef: ElementRef, private renderer: Renderer2) {}

  public ngAfterViewInit(): void {
    this.insertSpan(this.elementRef.nativeElement.childNodes, this.renderer);
  }

  private insertSpan(nodes: NodeList, renderer: Renderer2): void {
    nodes.forEach(node => {
      if (node.nodeName === '#text') {
        const span = renderer.createElement('span');
        const parent = renderer.parentNode(node);
        renderer.insertBefore(parent, span, node);
        renderer.appendChild(span, node);
      }
    });
  }
}
