import { ChangeDetectionStrategy, Component, Input, Renderer2, ViewEncapsulation } from '@angular/core';
import { BaseInput, DomListenerUnbinder } from 'src/lib/classes/base-input.class';

@Component({
  selector: 'tdm-text-area',
  templateUrl: './text-area.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class TextAreaComponent extends BaseInput {
  @Input() public type: string;
  @Input() public placeholder: string;
  @Input() public rows: number = 3;
  @Input() public cols: number;

  @Input()
  public error: boolean = false;

  @Input()
  public success: boolean = false;

  @Input()
  public clearable: boolean = false;

  /**
   * Binds DOM event listeners
   * @returns Function to unbind listeners on destroy
   */
  protected addDomListeners(renderer: Renderer2): DomListenerUnbinder {
    return this.addBlurAndInputListeners(renderer);
  }
}
