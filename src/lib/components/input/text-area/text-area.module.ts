import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextAreaComponent } from './text-area.component';
import { InputContainerModule } from '../input-container/input-container.module';

@NgModule({
  declarations: [TextAreaComponent],
  imports: [CommonModule, InputContainerModule],
  exports: [TextAreaComponent]
})
export class TextAreaModule {}
