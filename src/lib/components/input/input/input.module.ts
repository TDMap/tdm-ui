import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './input.component';
import { InputContainerModule } from '../input-container/input-container.module';

@NgModule({
  declarations: [InputComponent],
  imports: [CommonModule, InputContainerModule],
  exports: [InputComponent]
})
export class InputModule {}
