import { ChangeDetectionStrategy, Component, Input, Renderer2, ViewEncapsulation } from '@angular/core';
import { VARCHAR_SIZE } from 'src/lib/models/input.models';
import { BaseInput, DomListenerUnbinder } from '../../../classes/base-input.class';

@Component({
  selector: 'tdm-input',
  templateUrl: './input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None
})
export class InputComponent extends BaseInput {
  @Input() type: string;
  @Input() maxlength: number = VARCHAR_SIZE;
  @Input() placeholder: string;

  @Input()
  public error: boolean = false;

  @Input()
  public success: boolean = false;

  @Input()
  public clearable: boolean = false;

  /**
   * Binds DOM event listeners
   * @returns Function to unbind listeners on destroy
   */
  protected addDomListeners(renderer: Renderer2): DomListenerUnbinder {
    return this.addBlurAndInputListeners(renderer);
  }
}
