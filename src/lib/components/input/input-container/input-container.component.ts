import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'tdm-input-container',
  templateUrl: './input-container.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: { class: 'input-container' }
})
export class InputContainerComponent {
  @Input() public errorMessage: string;
  @Input() public label: string;
  @Input() public required: boolean = false;
  @Input() public clearable: boolean = false;

  @Input()
  @HostBinding('class.disabled')
  protected disabled: boolean = false;

  @Input()
  @HostBinding('class.focused')
  protected focused: boolean = false;

  @Input()
  @HostBinding('class.readonly')
  protected readonly: boolean = false;

  @Input()
  @HostBinding('class.error')
  protected error: boolean = false;

  @Input()
  @HostBinding('class.success')
  protected success: boolean = false;

  @Output() onClearClick: EventEmitter<void> = new EventEmitter();
}
