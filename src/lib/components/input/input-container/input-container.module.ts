import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputContainerComponent } from './input-container.component';

@NgModule({
  declarations: [InputContainerComponent],
  imports: [CommonModule],
  exports: [InputContainerComponent]
})
export class InputContainerModule {}
