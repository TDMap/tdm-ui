import {
  ChangeDetectorRef,
  Directive,
  ElementRef,
  EventEmitter,
  HostBinding,
  Injector,
  Input,
  OnDestroy,
  Optional,
  Output,
  Renderer2,
  Self,
  ViewChild
} from '@angular/core';
import { AbstractControl, ControlValueAccessor, NgControl } from '@angular/forms';

type OnChange = (value: any) => undefined;
export type DomListenerUnbinder = () => void;

const onChange: OnChange = (value: any) => undefined;
const onTouched = () => undefined;

@Directive()
export abstract class BaseInput implements ControlValueAccessor, OnDestroy {
  @Input() label: string;
  @Input() tabindex: number;
  @Input() formControl: AbstractControl;

  @ViewChild('input', { static: true }) inputElementRef: ElementRef;

  @Input()
  @HostBinding('class.readonly')
  readonly: boolean;

  @Output() public readonly blurred: EventEmitter<void> = new EventEmitter<void>();
  @Output() public readonly focused: EventEmitter<void> = new EventEmitter<void>();

  public errorMessage: string = null;
  public isFocused: boolean = false;
  public disabled: boolean = false;
  public onChange: OnChange = onChange;
  public required: boolean;
  public value: any;

  protected readonly cdr: ChangeDetectorRef;
  protected readonly elementRef: ElementRef;

  protected onTouched = onTouched;

  private unbindDomListeners: DomListenerUnbinder;
  /**
   * Creates instance
   */
  constructor(protected readonly injector: Injector, @Self() @Optional() public controlDirective: NgControl) {
    this.cdr = injector.get(ChangeDetectorRef);
    this.elementRef = injector.get(ElementRef);
    if (controlDirective) this.controlDirective.valueAccessor = this;
  }

  /**
   * Processes component destruction
   */
  ngOnDestroy(): void {
    this.unbindDomListeners && this.unbindDomListeners();
  }

  protected setRequiredState(required: boolean): void {
    this.required = required;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public setDisabledState(disabled: boolean): void {
    this.disabled = disabled;
    this.cdr.markForCheck();
  }

  public writeValue(value: any): void {
    this.value = value ? value : null;
    this.render();
  }

  public inputHandler(event: any): void {
    this.onChange(event.target.value);
  }

  public onFocusChange(focused: boolean): void {
    this.isFocused = focused;
    if (focused) {
      if (!this.unbindDomListeners) this.unbindDomListeners = this.addDomListeners(this.injector.get(Renderer2));

      this.onTouched();
      this.focused.emit();
    } else {
      this.blurred.emit();
    }
  }

  protected render(): void {
    if (this.inputElementRef) this.inputElementRef.nativeElement.value = this.value;
  }

  public focus(): void {
    const input: HTMLInputElement = this.inputElementRef && this.inputElementRef.nativeElement;

    if (input) {
      input.focus();
      input.select();
    }
  }

  public clear() {
    if (this.controlDirective) {
      this.controlDirective.reset();
    } else {
      this.writeValue(null);
    }
  }

  protected abstract addDomListeners(renderer: Renderer2): DomListenerUnbinder;

  protected addBlurAndInputListeners(renderer: Renderer2): DomListenerUnbinder {
    const input: HTMLElement = this.inputElementRef.nativeElement;
    const unbindBlurListener = renderer.listen(input, 'blur', () => {
      this.onFocusChange(false);
      this.cdr.markForCheck(); // to refresh view
    });
    const unbindInputListener = renderer.listen(input, 'input', this.inputHandler.bind(this));

    return () => {
      unbindBlurListener();
      unbindInputListener();
    };
  }
}
